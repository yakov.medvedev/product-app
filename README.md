# product-app

Task implemented by Yakov Medvedev for Awesome company.

Technologies:
- Java 8
- Spring Boot, Spring WEB
- H2 database
- Maven

Run the program:
1. run java -jar target/product-app-0.0.1-SNAPSHOT.jar
2. go to http://localhost:8080/h2-console/
User Name: admin
Password: admin

3. create and fill table 

CREATE TABLE product(
   id serial PRIMARY KEY,
   name VARCHAR (100) UNIQUE NOT NULL,
   description VARCHAR (1000) NOT NULL
);

INSERT INTO product (name, description)
VALUES 
('Gala', 'moushee'),
('Lancome ', 'PARFUM'),
('Tom Ford Oud', 'some stuff'),
('Steve Madden', 'Fabric upper or faux-leather upper.');

4. goto http://localhost:8080/shop/product?nameFilter=.\*T.*
there is no  ('Tom Ford Oud', 'some stuff') record present.