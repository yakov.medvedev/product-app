package com.eva.productapp.service;

import com.eva.productapp.model.Product;

import java.util.List;

public interface ProductService {
    List<Product> getProductsByRegexp(String regexp);
    List<Product> getProductsByRegexpWithLimit(String regexp, long limit);
}
