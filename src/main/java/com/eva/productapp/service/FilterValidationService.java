package com.eva.productapp.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FilterValidationService {

    private static String BASIC_REGEXP = "^.*[a-zA-Z]{1,100}.*$";

    public static boolean isValid(String regexp){
        if (regexp == null || regexp.length() == 0) return false;
        Pattern basicPattern = Pattern.compile(BASIC_REGEXP);
        Matcher matcher = basicPattern.matcher(regexp);
        if (!matcher.matches()) return false;
        return true;
    }
}
