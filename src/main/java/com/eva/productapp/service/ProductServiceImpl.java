package com.eva.productapp.service;

import com.eva.productapp.connection.H2Connector;
import com.eva.productapp.model.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Service
public class ProductServiceImpl implements ProductService {

    private long DEFAULT_LIMIT = 1000;
    private static Logger LOGGER = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Override
    public List<Product> getProductsByRegexp(String param) {
        return  getProductsByRegexpWithLimit(param, DEFAULT_LIMIT);
    }

    @Override
    public List<Product> getProductsByRegexpWithLimit(String param, long limit) {
        List<Product> list = new ArrayList<>();
        long newLimit = (limit > DEFAULT_LIMIT && limit < Long.MAX_VALUE) ? limit : DEFAULT_LIMIT;
        String sqlStr = "SELECT * FROM product LIMIT " + newLimit + ";";
        Pattern pattern = Pattern.compile(param);
        try (Statement statement= H2Connector.getConnection().createStatement()){
            ResultSet rs = statement.executeQuery(sqlStr);
            while (rs.next()) {
                String productName = rs.getString(2);
                if (!pattern.matcher(productName).matches()){
                    list.add(new Product(rs.getLong(1), productName, rs.getString(3)));
                }
            }
        } catch (SQLException e) {
            LOGGER.error("",e);
        }
        return list;
    }
}
