package com.eva.productapp.controllers;

import com.eva.productapp.model.Product;
import com.eva.productapp.service.FilterValidationService;
import com.eva.productapp.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.InvalidParameterException;
import java.util.List;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/shop/product", method = RequestMethod.GET, produces = { "application/json"})
        public List<Product> getProducts(@RequestParam String nameFilter) {
        if (!FilterValidationService.isValid(nameFilter)) throw new InvalidParameterException();
        return productService.getProductsByRegexp(nameFilter);
    }

    @RequestMapping(value = "/shop/product/{limit}/", method = RequestMethod.GET, produces = { "application/json"})
    public List<Product> getProductsWithLimit(@RequestParam String nameFilter, @PathVariable Long limit) {
        if (!FilterValidationService.isValid(nameFilter)) throw new InvalidParameterException();
        return productService.getProductsByRegexpWithLimit(nameFilter, limit);
    }
}
