package com.eva.productapp.connection;

import org.apache.commons.dbcp2.cpdsadapter.DriverAdapterCPDS;
import org.apache.commons.dbcp2.datasources.SharedPoolDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class H2Connector {

    private static final String url = "jdbc:h2:~/eva_db";
    private static final String user = "admin";
    private static final String password = "admin";
    private static Connection connection;
    private static DataSource ds;
    private static Logger LOGGER = LoggerFactory.getLogger(H2Connector.class);

    static {
        try(SharedPoolDataSource tds = new SharedPoolDataSource()) {
            DriverAdapterCPDS cpds = new DriverAdapterCPDS();
            cpds.setDriver(Class.forName("org.h2.Driver").getName());
            cpds.setUrl(url);
            cpds.setUser(user);
            cpds.setPassword(password);
            tds.setConnectionPoolDataSource(cpds);
            tds.setMaxTotal(30);
            tds.setDefaultMaxWaitMillis(1000);
            ds = tds;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() {
        try {
            if (connection == null ) connection = ds.getConnection();
        } catch (SQLException e) {
            LOGGER.error("Connection failed.");
            e.printStackTrace();
        }
        return connection;
    }
}
