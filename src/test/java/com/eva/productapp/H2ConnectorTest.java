package com.eva.productapp;

import com.eva.productapp.connection.H2Connector;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

public class H2ConnectorTest {

    @Test
    public void getConnection() throws SQLException {
        Assert.assertNotNull(H2Connector.getConnection());
    }
}
